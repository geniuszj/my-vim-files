" genzj <zj0512@gmail.com>
" contains miscellaneous functions

if exists("g:genzj_scraps_loaded")
    finish
endif
let g:genzj_scraps_loaded = 1

" build_go_files is a custom function that builds or compiles the test file.
" It calls :GoBuild if its a Go file, or :GoTestCompile if it's a test file
function! scraps#Build_go_files()
    let l:file = expand('%')
    if l:file =~# '^\f\+_test\.go$'
        call go#cmd#Test(0, 1)
    elseif l:file =~# '^\f\+\.go$'
        call go#cmd#Build(0)
    endif
endfunction

" ToggleQuickfixErrorWindow toggles error quickfix window
function! scraps#ToggleQuickfixErrorWindow()
    if empty(filter(tabpagebuflist(), 'getbufvar(v:val, "&buftype") is# "quickfix"'))
        let sum = eval(join(values(lsp#get_buffer_diagnostics_counts()), '+'))
        if sum > 0
            let t:scraps_LspDocumentDiagnosticsOpened = 1
            LspDocumentDiagnostics
        else
            " No location/quickfix list shown, open syntastic error location panel
            copen
        endif
    else
        if exists("t:scraps_LspDocumentDiagnosticsOpened") && t:scraps_LspDocumentDiagnosticsOpened
            let t:scraps_LspDocumentDiagnosticsOpened = 0
            lclose
        endif
        cclose
    endif
endfunction

function! scraps#CloseVista(tid)
    execute 'Vista!'
endfunction

function! scraps#ToggleVista()
    if !exists('*g:vista.winnr()') || g:vista.winnr() == -1
        execute 'Vista vim_lsp'
    else
        " must be executed asynchronously
        let tid = timer_start(50, 'scraps#CloseVista')
        unlet tid
    endif
endfunction

function! scraps#VimPyVenv() abort
    for l:full_path in [expand($WORKON_HOME . "/vimenv/"), expand("~/.venvs/vimenv/")]
        if l:full_path->isdirectory()
            return l:full_path
        endif
    endfor
    return ""
endfunction

function! scraps#InjectNeovim() abort
    try
        pythonx import neovim
        echom "builtin python supports neovim"
        return ""
    catch
        "echom "builtin python doesn't support neovim, looking for venv"
    endtry

    let l:venv = scraps#VimPyVenv()
    if l:venv == ""
        echohl WarningMsg
        echom "No virtualenv for vim found, abort" . $WORKON_HOME
        echohl None
        return ""
    else
        " echom "found venv" . l:venv
    endif

    let l:savepath = $PATH
    if has('win32')
        let l:activate_script = escape(l:venv . "Scripts\\activate_this.py", '\')
        call pyxeval("exec(open('" . l:activate_script . "').read(), {'__file__': '" . l:activate_script . "'})")
        let g:python3_host_prog = l:venv . "Scripts\\python.exe"
    elseif has('linux') || has('mac')
        let $PATH = l:venv . "bin/:" . $PATH
        let g:python3_host_prog = l:venv . "bin/python"
        let l:site_packages = globpath(l:venv, 'lib/*/site-packages', 0, 1)
        pythonx import sys
        call pyxeval('sys.path.append("' . l:site_packages[0] . '")')
    else
        let $PATH = l:venv . "bin/:" . $PATH
    endif
    " echom $PATH
    return l:savepath
endfunction

function! scraps#RestoreNeovim(savepath) abort
    if a:savepath
        let $PATH = a:savepath
    endif
endfunction

function! scraps#InitializeDenite() abort
    let l:savepath = scraps#InjectNeovim()
    try
        call denite#initialize()
    catch
        echom "denite initialization failed"
        echom v:exception
    endtry

    " TODO move this to a separate function
    " try powerline in virtualenv
	if has("gui_running")
        if !g:myvimrc#has_powerline
            call EnablePowerline()
        endif
    endif
    call scraps#RestoreNeovim(l:savepath)

	" Define mappings
	function! s:denite_my_settings() abort
	  nnoremap <silent><buffer><expr> <CR>
	  \ denite#do_map('do_action')
	  nnoremap <silent><buffer><expr> d
	  \ denite#do_map('do_action', 'delete')
	  nnoremap <silent><buffer><expr> p
	  \ denite#do_map('do_action', 'preview')
	  nnoremap <silent><buffer><expr> q
	  \ denite#do_map('quit')
	  nnoremap <silent><buffer><expr> i
	  \ denite#do_map('open_filter_buffer')
	  nnoremap <silent><buffer><expr> /
	  \ denite#do_map('open_filter_buffer')
	  nnoremap <silent><buffer><expr> <Space>
	  \ denite#do_map('toggle_select').'j'
	endfunction

	function! s:denite_filter_my_settings() abort
	  imap <silent><buffer> <C-o> <Plug>(denite_filter_quit)
	endfunction

    augroup my_denite
        autocmd!
        autocmd FileType denite call s:denite_my_settings()
        autocmd FileType denite-filter call s:denite_filter_my_settings()
    augroup END

	" Change file/rec command.
    call system("rg --version")
    if !v:shell_error
        " Ripgrep command on grep source
        call denite#custom#var('grep', {
                    \ 'command': ['rg'],
                    \ 'default_opts': ['-i', '--vimgrep', '--no-heading'],
                    \ 'recursive_opts': [],
                    \ 'pattern_opt': ['--regexp'],
                    \ 'separator': ['--'],
                    \ 'final_opts': [],
                    \ })
        call denite#custom#var('file/rec', 'command',
        \ ['rg', '--hidden', '--files', '--glob', '!.git', '--color', 'never'])
    endif

    call system("fd --version")
    if !v:shell_error
        call denite#custom#var('file/rec', 'command',
                    \ ['fd', '--follow', '--hidden', '-t', 'f',
                    \ '-t', 'l', '--color', 'never', '--exclude', '\.git',
                    \ '--search-path' ])
        call denite#custom#var('directory_rec', 'command',
                    \ ['fd', '--follow', '--hidden', '-t', 'd',
                    \ '--color', 'never', '--exclude', '\.git',
                    \ '--search-path' ])
    endif
endfunction

