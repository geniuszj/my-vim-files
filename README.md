# My gVim Files

## Prerequisites

* Vim 8.2+
* Mainstream Linux (tested on Arch with Xfce) distributions and macOS Catalina
  1. Check Python version that the vim is built with by executing
     `vim -s <(echo ':pythonx import sys; print(sys.version)')`
  1. Install the matching Python version with shared library, pyenv is
     recommended and installation with it can be done like:
     `env PYTHON_CONFIGURE_OPTS="--enable-shared" pyenv install 3.x.y`
* Win10
  * Python >3.6

## Installation:

```sh
# 1. create a python virtualenv
#   If virtualenvwrapper is installed (recommended):
mkvirtualenv vimenv && workon vimenv
#   Or manually create the virtual-env in ~/.venvs
mkdir -p ~/.venvs ; virtualenv ~/.venvs/vimenv && source ~/.venvs/vimenv/bin/activate

# 2. install python dependencies
pip install powerline-status msgpack pynvim

# 3. clone the scripts
#   Windows:
cd path-to-vim
git clone git@bitbucket.org:geniuszj/my-vim-files.git vimfiles
cd vimfiles
git submodule update --init --recursive
#   Linux:
mv ~/.vim ~/.vim-bak 2>/dev/null
git clone git@bitbucket.org:geniuszj/my-vim-files.git ~/.vim
cd ~/.vim
git submodule update --init --recursive
```

To activate the scripts, create/update vimrc file (`~/.vimrc` in Linux and
`_vimrc` in Windows) with following content (showing the Linux version):

```vimscript
" required only when working together with pyenv
set pythonthreedll=~/.pyenv/versions/3.7.10/lib/libpython3.7m.so

source ~/.vim/myvimrc
set background=dark
call EnablePowerline()
```

## Content

### Generic:

* my vim configuration `myvimrc` (don't forget to source it from `.vimrc` or  `_vimrc`);

### Syntax & compiler:

* *markdown* syntax;
* *python3* compiler

### Plugins:

* [pathogen](https://github.com/tpope/vim-pathogen) for bundle management.
* [grep](http://www.vim.org/scripts/script.php?script_id=311) Grep tools integration.

### Theme

* A very nerd color theme [nerv-ous](http://www.vim.org/scripts/script.php?script_id=4764)(refined a little for *Pmenu* and *Todo*).
* Colorscheme mainly designed for Python scripts tty. [pychimp](https://github.com/Pychimp/Pychimp-vim)
* A great high contrast colorscheme [badwolf](http://stevelosh.com/projects/badwolf/)

## As bundles:

### Theme & Appearance

* [vim-solarized8](https://github.com/lifepillar/vim-solarized8);
* A light colorscheme [summerfruit256](https://github.com/vim-scripts/summerfruit256.vim.git)
* [Vim Devicons](https://github.com/ryanoasis/vim-devicons) adds icons to many plugins, needs nerdfont patched fonts

### Syntax & compiler:

* [syntastic](https://github.com/scrooloose/syntastic) for syntax validation of many languages, replace pyflakes-vim;
* [plantuml-syntax](https://github.com/aklt/plantuml-syntax) for [plantuml](http://plantuml.com/) diagrams.
* [vim-go](https://github.com/fatih/vim-go) Golang support. Read `help vim-go` for binary installation help.
* [vim-css-color](https://github.com/skammer/vim-css-color)
* [vim-json](https://github.com/elzr/vim-json)
* [vim-less](https://github.com/groenewege/vim-less) for CSS highlight (including color code) and LESS support.
* [Colorizer](https://github.com/chrisbra/Colorizer) color hex codes and color names, highlight ANSI terminal color sequences correctly
* [vim-fish](https://github.com/dag/vim-fish) an addon for Vim providing support for editing [fish](https://github.com/fish-shell/fish-shell) scripts.

#### Language Server Protocol supports:

* [vim-lsp](https://github.com/prabirshrestha/vim-lsp) and [vim-lsp-settings](https://github.com/mattn/vim-lsp-settings) for Language Server Protocol supports
* [asyncomplete](https://github.com/prabirshrestha/asyncomplete.vim) and its source for vim-lsp [asyncomplete-lsp](https://github.com/prabirshrestha/asyncomplete-lsp.vim). (Currently asyncomplete is only enabled in LSP supported documents.)
* Use [vista.vim](https://github.com/liuchengxu/vista.vim) for tags in LSP supported documents.

### Plugins:

* [NERD Tree](https://github.com/scrooloose/nerdtree) great embedded file explorer;

* [Tabular](https://github.com/godlygeek/tabular) mainly used for ASCII art;

* [Calendar](http://www.vim.org/scripts/script.php?script_id=52) check calendar, with diary and todo-list function which I haven't tried.

* [Mark](http://www.vim.org/scripts/script.php?script_id=2666) highlight multi keywords simultaneously (release vimball manually, not as git repository).

* [emmet-vim](https://github.com/mattn/emmet-vim) `zencoding` for quick HTML edit.

* [vim-fugitive](https://github.com/tpope/vim-fugitive) git operation plugin

* [vim-virtualenv](https://github.com/jmcantrell/vim-virtualenv) VirtualEnv wrapper in vim. To enable the indicator in
  Powerline, add following lines to `powerline_path/config_files/colorschemes/vim/themes/default.json`:

        {
            "function": "powerline.segments.common.env.virtualenv",
            "before": "@",
            "priority": 20
        }

* [tagbar](https://github.com/majutsushi/tagbar) alteration of taglist
  offering tree-like tag lists.
  
* [The NERD Commenter](https://github.com/scrooloose/nerdcommenter) nice comment toggling script.

* [denite.nvim](https://github.com/Shougo/denite.nvim) Dark powered asynchronous unite all interfaces for Neovim/Vim8

* [rooter](https://github.com/airblade/vim-rooter) changes CWD to project folder automatically

## And assistant programs:

* [ctags 5.8 for win32](http://ctags.sourceforge.net/).
* GVIM fullscreen support into Win(copy `dll/*` into the folder containing `gvim.exe`)

## Patches:

Includes flaw fixes for specific platform and customization of bundle plugins. In long term these patches should be commited to fork projects.

