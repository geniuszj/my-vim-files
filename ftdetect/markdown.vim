" Markdown
" autocmd BufNewFile,BufRead *.{md,mkd,mkdn,mark*} set filetype=markdown
" support pandoc extended markdown syntax
autocmd BufNewFile,BufRead *.{md,mkd,mkdn,mark*} set filetype=pandoc
